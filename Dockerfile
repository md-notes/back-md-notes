FROM keymetrics/pm2:14-stretch
ADD . /app/
WORKDIR /app
RUN npm install
ENV LANG fr_FR.UTF-8
ENV LANGUAGE fr_FR:fr
ENV LC_ALL fr_FR.UTF-8
EXPOSE 80
