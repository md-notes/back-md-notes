{
  "openapi": "3.0.0",
  "info": {
    "title": "md-notes API",
    "description": "Swagger for md-notes API",
    "termsOfService": "",
    "contact": {
      "name": "Bastien FOUQUET",
      "url": "https://gitlab.com/bastienFouquet",
      "email": "bastien.fouquet85@gmail.com"
    },
    "license": {
      "name": "Apache 2.0",
      "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
    },
    "version": "1.0.0"
  },
  "servers": [
    {
      "url": "http://mdnotes.ninja:8080/"
    },
    {
      "url": "http://mdnotes.ninja:8081/"
    },
    {
      "url": "http://localhost:1337/"
    }
  ],
  "externalDocs": {
    "url": "https://gitlab.com/md-notes"
  },
  "tags": [
    {
      "name": "Leaf",
      "description": "Sails blueprint actions for the **Leaf** model"
    },
    {
      "name": "Note",
      "description": "Sails blueprint actions for the **Note** model"
    },
    {
      "name": "User",
      "description": "Sails blueprint actions for the **User** model"
    }
  ],
  "components": {
    "schemas": {
      "leaf": {
        "type": "object",
        "allOf": [
          {
            "$ref": "#/components/schemas/leaf-without-required-constraint"
          },
          {
            "required": [
              "id",
              "label",
              "user"
            ]
          }
        ]
      },
      "leaf-without-required-constraint": {
        "type": "object",
        "description": "Sails ORM Model **Leaf**",
        "properties": {
          "id": {
            "type": "string",
            "uniqueItems": true
          },
          "label": {
            "type": "string"
          },
          "parentLeaf": {
            "description": "JSON dictionary representing the **leaf** instance or FK when creating / updating / not populated",
            "oneOf": [
              {
                "$ref": "#/components/schemas/leaf"
              }
            ]
          },
          "children": {
            "description": "Array of **leaf**'s or array of FK's when creating / updating / not populated",
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/leaf"
            }
          },
          "user": {
            "description": "JSON dictionary representing the **user** instance or FK when creating / updating / not populated",
            "oneOf": [
              {
                "$ref": "#/components/schemas/user"
              }
            ]
          },
          "notes": {
            "description": "Array of **note**'s or array of FK's when creating / updating / not populated",
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/note"
            }
          }
        }
      },
      "note": {
        "type": "object",
        "allOf": [
          {
            "$ref": "#/components/schemas/note-without-required-constraint"
          },
          {
            "required": [
              "id",
              "title",
              "user"
            ]
          }
        ]
      },
      "note-without-required-constraint": {
        "type": "object",
        "description": "Sails ORM Model **Note**",
        "properties": {
          "id": {
            "type": "string",
            "uniqueItems": true
          },
          "title": {
            "type": "string"
          },
          "content": {
            "type": "string",
            "nullable": true
          },
          "user": {
            "description": "JSON dictionary representing the **user** instance or FK when creating / updating / not populated",
            "oneOf": [
              {
                "$ref": "#/components/schemas/user"
              }
            ]
          },
          "leaf": {
            "description": "JSON dictionary representing the **leaf** instance or FK when creating / updating / not populated",
            "oneOf": [
              {
                "$ref": "#/components/schemas/leaf"
              }
            ]
          }
        }
      },
      "user": {
        "type": "object",
        "allOf": [
          {
            "$ref": "#/components/schemas/user-without-required-constraint"
          },
          {
            "required": [
              "id",
              "login",
              "password"
            ]
          }
        ]
      },
      "user-without-required-constraint": {
        "type": "object",
        "description": "Sails ORM Model **User**",
        "properties": {
          "id": {
            "type": "string",
            "uniqueItems": true
          },
          "login": {
            "type": "string",
            "uniqueItems": true
          },
          "password": {
            "type": "string"
          }
        }
      },
      "leaf-unpopulated": {
        "properties": {
          "id": {
            "type": "string"
          },
          "label": {
            "type": "string"
          },
          "parentLeaf": {
            "type": "string"
          },
          "user": {
            "type": "string"
          }
        }
      },
      "note-unpopulated": {
        "properties": {
          "id": {
            "type": "string"
          },
          "title": {
            "type": "string"
          },
          "content": {
            "type": "string"
          },
          "leaf": {
            "type": "string"
          },
          "user": {
            "type": "string"
          }
        }
      },
      "user-without-password": {
        "properties": {
          "id": {
            "type": "string"
          },
          "login": {
            "type": "string"
          }
        }
      }
    },
    "parameters": {
      "ModelPKParam-note": {
        "in": "path",
        "name": "_id",
        "required": true,
        "schema": {
          "type": "string",
          "uniqueItems": true
        },
        "description": "The desired **Note** record's primary key value"
      },
      "ModelPKParam-leaf": {
        "in": "path",
        "name": "_id",
        "required": true,
        "schema": {
          "type": "string",
          "uniqueItems": true
        },
        "description": "The desired **Leaf** record's primary key value"
      },
      "AttributeFilterParam": {
        "in": "query",
        "name": "_*_",
        "required": false,
        "schema": {
          "type": "string"
        },
        "description": "To filter results based on a particular attribute, specify a query parameter with the same name as the attribute defined on your model. For instance, if our `Purchase` model has an `amount` attribute, we could send `GET /purchase?amount=99.99` to return a list of $99.99 purchases."
      },
      "WhereQueryParam": {
        "in": "query",
        "name": "where",
        "required": false,
        "schema": {
          "type": "string"
        },
        "description": "Instead of filtering based on a specific attribute, you may instead choose to provide a `where` parameter with the WHERE piece of a [Waterline criteria](https://sailsjs.com/documentation/concepts/models-and-orm/query-language), _encoded as a JSON string_. This allows you to take advantage of `contains`, `startsWith`, and other sub-attribute criteria modifiers for more powerful `find()` queries.\n\ne.g. `?where={\"name\":{\"contains\":\"theodore\"}}`"
      },
      "LimitQueryParam": {
        "in": "query",
        "name": "limit",
        "required": false,
        "schema": {
          "type": "integer"
        },
        "description": "The maximum number of records to send back (useful for pagination). Defaults to 30."
      },
      "SkipQueryParam": {
        "in": "query",
        "name": "skip",
        "required": false,
        "schema": {
          "type": "integer"
        },
        "description": "The number of records to skip (useful for pagination)."
      },
      "SortQueryParam": {
        "in": "query",
        "name": "sort",
        "required": false,
        "schema": {
          "type": "string"
        },
        "description": "The sort order. By default, returned records are sorted by primary key value in ascending order.\n\ne.g. `?sort=lastName%20ASC`"
      }
    },
    "securitySchemes": {
      "Connected": {
        "type": "apiKey",
        "in": "header",
        "name": "Authorization"
      }
    }
  },
  "paths": {
    "/auth": {
      "post": {
        "summary": "Auth User",
        "description": "Authenticate a User by login/password",
        "externalDocs": null,
        "tags": [
          "User"
        ],
        "parameters": [],
        "responses": {
          "200": {
            "description": "The requested resource",
            "content": {
              "application/json": {
                "schema": {
                  "properties": {
                    "token": {
                      "type": "string"
                    },
                    "user": {
                      "$ref": "#/components/schemas/user-without-password"
                    }
                  }
                }
              }
            }
          },
          "400": {
            "description": "Bad request"
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Resource not found"
          },
          "500": {
            "description": "Internal server error"
          }
        },
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "properties": {
                  "login": {
                    "type": "string",
                    "required": true
                  },
                  "password": {
                    "type": "string",
                    "format": "password",
                    "required": true
                  }
                }
              }
            }
          }
        }
      }
    },
    "/users": {
      "post": {
        "summary": "Create User",
        "description": "Create a User with login/password",
        "externalDocs": null,
        "tags": [
          "User"
        ],
        "parameters": [],
        "responses": {
          "200": {
            "description": "The requested resource",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/user-without-password"
                }
              }
            }
          },
          "400": {
            "description": "Validation errors; details in JSON response"
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Resource not found"
          },
          "500": {
            "description": "Internal server error"
          }
        },
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "properties": {
                  "login": {
                    "type": "string",
                    "required": true
                  },
                  "password": {
                    "type": "string",
                    "format": "password",
                    "required": true
                  }
                }
              }
            }
          }
        }
      }
    },
    "/notes": {
      "post": {
        "summary": "Create Note",
        "description": "Create a new Note record linked to your account",
        "externalDocs": null,
        "tags": [
          "Note"
        ],
        "parameters": [],
        "responses": {
          "200": {
            "description": "The requested resource",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/note-unpopulated"
                }
              }
            }
          },
          "400": {
            "description": "Validation errors; details in JSON response"
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Resource not found"
          },
          "500": {
            "description": "Internal server error"
          }
        },
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "type": "object",
                "properties": {
                  "title": {
                    "type": "string",
                    "required": true
                  },
                  "leaf": {
                    "type": "string",
                    "required": true
                  },
                  "content": {
                    "type": "string",
                    "format": "markdown"
                  }
                }
              }
            }
          }
        },
        "security": [
          {
            "Connected": []
          }
        ]
      }
    },
    "/leaf": {
      "post": {
        "summary": "Create Leaf",
        "description": "Create a new Leaf record linked to your account",
        "externalDocs": null,
        "tags": [
          "Leaf"
        ],
        "parameters": [],
        "responses": {
          "200": {
            "description": "The requested resource",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/leaf-unpopulated"
                }
              }
            }
          },
          "400": {
            "description": "Validation errors; details in JSON response"
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Resource not found"
          },
          "500": {
            "description": "Internal server error"
          }
        },
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "type": "object",
                "properties": {
                  "label": {
                    "type": "string",
                    "required": true
                  },
                  "parentLeaf": {
                    "type": "string"
                  }
                }
              }
            }
          }
        },
        "security": [
          {
            "Connected": []
          }
        ]
      }
    },
    "/tree": {
      "get": {
        "summary": "Get all parents Leaf",
        "description": "Get parents Leaf (leaf with no parentLeaf) of your account",
        "externalDocs": null,
        "tags": [
          "Leaf"
        ],
        "parameters": [],
        "responses": {
          "200": {
            "description": "The requested resource",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/leaf-unpopulated"
                  }
                }
              }
            }
          },
          "400": {
            "description": "Bad request"
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Resource not found"
          },
          "500": {
            "description": "Internal server error"
          }
        },
        "security": [
          {
            "Connected": []
          }
        ]
      }
    },
    "/notes/{_id}": {
      "put": {
        "summary": "Update Note",
        "description": "Update a new Note record",
        "externalDocs": null,
        "tags": [
          "Note"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/ModelPKParam-note"
          }
        ],
        "responses": {
          "200": {
            "description": "The requested resource",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/note-unpopulated"
                }
              }
            }
          },
          "400": {
            "description": "Validation errors; details in JSON response"
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Cannot update, **Note** record with specified ID **NOT** found"
          },
          "500": {
            "description": "Internal server error"
          }
        },
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "type": "object",
                "properties": {
                  "title": {
                    "type": "string"
                  },
                  "leaf": {
                    "type": "string"
                  },
                  "content": {
                    "type": "string",
                    "format": "markdown"
                  }
                }
              }
            }
          }
        },
        "security": [
          {
            "Connected": []
          }
        ]
      },
      "delete": {
        "summary": "Delete Note",
        "description": "Delete a new Note record with id",
        "externalDocs": null,
        "tags": [
          "Note"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "_id",
            "required": true,
            "schema": {
              "type": "string"
            },
            "description": "Route pattern variable `_id`"
          }
        ],
        "responses": {
          "200": {
            "description": "The requested resource"
          },
          "400": {
            "description": "Bad request"
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Resource not found"
          },
          "500": {
            "description": "Internal server error"
          }
        },
        "security": [
          {
            "Connected": []
          }
        ]
      },
      "get": {
        "summary": "Get Note",
        "description": "Get a new Note record with id",
        "externalDocs": null,
        "tags": [
          "Note"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "_id",
            "required": true,
            "schema": {
              "type": "string"
            },
            "description": "Route pattern variable `_id`"
          }
        ],
        "responses": {
          "200": {
            "description": "The requested resource",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/note-unpopulated"
                }
              }
            }
          },
          "400": {
            "description": "Bad request"
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Resource not found"
          },
          "500": {
            "description": "Internal server error"
          }
        },
        "security": [
          {
            "Connected": []
          }
        ]
      }
    },
    "/leaf/{_id}": {
      "put": {
        "summary": "Update Leaf",
        "description": "Update a new Leaf record linked to your account",
        "externalDocs": null,
        "tags": [
          "Leaf"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/ModelPKParam-leaf"
          }
        ],
        "responses": {
          "200": {
            "description": "The requested resource",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/leaf-unpopulated"
                }
              }
            }
          },
          "400": {
            "description": "Validation errors; details in JSON response"
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Cannot update, **Leaf** record with specified ID **NOT** found"
          },
          "500": {
            "description": "Internal server error"
          }
        },
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "type": "object",
                "properties": {
                  "label": {
                    "type": "string"
                  },
                  "parentLeaf": {
                    "type": "string"
                  }
                }
              }
            }
          }
        },
        "security": [
          {
            "Connected": []
          }
        ]
      },
      "delete": {
        "summary": "Delete Leaf",
        "description": "Delete a new Leaf record with id",
        "externalDocs": null,
        "tags": [
          "Leaf"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "_id",
            "required": true,
            "schema": {
              "type": "string"
            },
            "description": "Route pattern variable `_id`"
          }
        ],
        "responses": {
          "200": {
            "description": "The requested resource"
          },
          "400": {
            "description": "Bad request"
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Resource not found"
          },
          "500": {
            "description": "Internal server error"
          }
        },
        "security": [
          {
            "Connected": []
          }
        ]
      },
      "get": {
        "summary": "Get Leaf",
        "description": "Get Leaf with id",
        "externalDocs": null,
        "tags": [
          "Leaf"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "_id",
            "required": true,
            "schema": {
              "type": "string"
            },
            "description": "Route pattern variable `_id`"
          }
        ],
        "responses": {
          "200": {
            "description": "The requested resource",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/leaf"
                }
              }
            }
          },
          "400": {
            "description": "Bad request"
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Resource not found"
          },
          "500": {
            "description": "Internal server error"
          }
        },
        "security": [
          {
            "Connected": []
          }
        ]
      }
    }
  }
}