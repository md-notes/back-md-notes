const supertest = require('supertest');
const assert = require('assert');

describe('UserController', () => {
  after(async () => {
    await User.destroy({login: 'test'});
  });
  describe('#create', () => {
    it('should auth create', async () => {
      const res = await supertest(sails.hooks.http.app)
        .post('/users')
        .send({login: 'test', password: 'test'})
        .expect(200);
      assert(res.body);
      assert(res.body.id);
      assert(res.body.login);
    });
  });
  describe('#auth', () => {
    it('should auth user', async () => {
      const res = await supertest(sails.hooks.http.app)
        .post('/auth')
        .send({login: 'test', password: 'test'})
        .expect(200);
      assert(res.body);
      assert(res.body.token);
      assert(res.body.user.id);
      assert(res.body.user.login);
    });
  });
});

