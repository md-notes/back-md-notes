# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.4](https://gitlab.com/md-notes/back-md-notes/compare/v0.0.3...v0.0.4) (2022-01-06)


### Bug Fixes

* **db:** unique values ([e12ff93](https://gitlab.com/md-notes/back-md-notes/commit/e12ff9305591af72ee384ef6ba53f9ad43ea5be2))

### [0.0.3](https://gitlab.com/md-notes/back-md-notes/compare/v0.0.2...v0.0.3) (2022-01-05)

### 0.0.2 (2022-01-05)

### 0.0.1 (2022-01-05)
