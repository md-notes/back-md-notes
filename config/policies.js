/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {

  /***************************************************************************
   *                                                                          *
   * Default policy for all controllers and actions, unless overridden.       *
   * (`true` allows public access)                                            *
   *                                                                          *
   ***************************************************************************/

  '*': true,

  UserController: {
    'auth': true,
    'create': true
  },

  NoteController: {
    'create': ['isConnected'],
    'update': ['isConnected'],
    'delete': ['isConnected'],
    'getOne': ['isConnected']
  },

  LeafController: {
    'create': ['isConnected'],
    'update': ['isConnected'],
    'delete': ['isConnected'],
    'getAllParents': ['isConnected'],
    'getOne': ['isConnected']
  }

};
