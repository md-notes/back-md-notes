module.exports['swagger-generator'] = {
  disable: false,
  swaggerJsonPath: './swagger/swagger.json',
  defaults: {
    pathsToIgnore: ['api/v1/'],
    responses: {
      '200': {
        description: 'The requested resource'
      },
      '400': {
        description: 'Bad request'
      },
      '401': {
        description: 'Unauthorized'
      },
      '403': {
        description: 'Forbidden'
      },
      '404': {
        description: 'Resource not found'
      },
      '500': {
        description: 'Internal server error'
      }
    },
  },
  swagger: {
    openapi: '3.0.0',
    info: {
      title: 'md-notes API',
      description: 'Swagger for md-notes API',
      termsOfService: '',
      contact: {
        name: 'Bastien FOUQUET',
        url: 'https://gitlab.com/bastienFouquet',
        email: 'bastien.fouquet85@gmail.com'
      },
      license: {
        name: 'Apache 2.0',
        url: 'http://www.apache.org/licenses/LICENSE-2.0.html'
      },
      version: '1.0.0'
    },
    servers: [
      {url: 'http://mdnotes.ninja:8080/'},
      {url: 'http://mdnotes.ninja:8081/'},
      {url: 'http://localhost:1337/'}
    ],
    externalDocs: {
      url: 'https://gitlab.com/md-notes'
    },
  },
  excludeDeprecatedPutBlueprintRoutes: false,
  includeRoute: (routeInfo) => {
    if (routeInfo && routeInfo.path && routeInfo.path === '/swagger') {
      return false;
    }
    return true;
  }
};
