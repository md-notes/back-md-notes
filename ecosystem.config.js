module.exports = {
  apps: [{
    name: 'back-md-notes',
    script: './app.js',
    instances: 2,
    autorestart: true,
    watch: false,
    // eslint-disable-next-line camelcase
    exec_mode: 'cluster',
    // eslint-disable-next-line camelcase
    env_production: {
      NODE_ENV: 'production'
    },
    // eslint-disable-next-line camelcase
    env_development: {
      NODE_ENV: 'development'
    },
    // eslint-disable-next-line camelcase
    env_staging: {
      NODE_ENV: 'staging'
    }
  }]
};
