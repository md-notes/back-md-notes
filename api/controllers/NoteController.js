/**
 * @controller
 * NoteController
 */

/**
 * @swagger
 *
 * /allActions:
 *  tags:
 *    - Note
 *  security:
 *    - Connected: []
 *
 * components:
 *  schemas:
 *    note-unpopulated:
 *      properties:
 *        id:
 *          type: string
 *        title:
 *          type: string
 *        content:
 *          type: string
 *        leaf:
 *          type: string
 *        user:
 *          type: string
 */

const {v4} = require('uuid');
module.exports = {
  /**
   * @swagger
   *
   * /create:
   *    summary: Create Note
   *    description: Create a new Note record linked to your account
   *    externalDocs:
   *    requestBody:
   *      content:
   *        application/json:
   *          schema:
   *            type: object
   *            properties:
   *              title:
   *                type: string
   *                required: true
   *              leaf:
   *                type: string
   *                required: true
   *              content:
   *                type: string
   *                format: markdown
   *    responses:
   *      200:
   *        description: The requested resource
   *        content:
   *          application/json:
   *            schema:
   *              $ref: '#/components/schemas/note-unpopulated'
   */
  create: async (req, res) => {
    try {
      if (req.body.leaf && req.body.title) {
        const note = await Note.create({
          id: v4(),
          title: req.body.title,
          content: req.body.content,
          leaf: req.body.leaf,
          user: req.connection.user.id
        }).fetch();
        if (note) {
          return res.json(note);
        } else {
          return res.notFound();
        }
      } else {
        return res.badRequest('Invalid fields');
      }
    } catch (e) {
      console.error(e);
      return res.serverError(e);
    }
  },
  /**
   * @swagger
   *
   * /update:
   *    summary: Update Note
   *    description: Update a new Note record
   *    externalDocs:
   *    requestBody:
   *      content:
   *        application/json:
   *          schema:
   *            type: object
   *            properties:
   *              title:
   *                type: string
   *              leaf:
   *                type: string
   *              content:
   *                type: string
   *                format: markdown
   *    responses:
   *      200:
   *        description: The requested resource
   *        content:
   *          application/json:
   *            schema:
   *              $ref: '#/components/schemas/note-unpopulated'
   */
  update: async (req, res) => {
    try {
      const note = await Note.updateOne({
        id: req.params.id
      }).set({
        title: req.body.title,
        content: req.body.content,
        leaf: req.body.leaf,
      });
      if (note) {
        return res.json(note);
      } else {
        return res.notFound();
      }
    } catch (e) {
      console.error(e);
      return res.serverError(e);
    }
  },
  /**
   * @swagger
   *
   * /delete:
   *    summary: Delete Note
   *    description: Delete a new Note record with id
   *    externalDocs:
   */
  delete: async (req, res) => {
    try {
      const isDeleted = await Note.destroyOne({
        id: req.params.id
      });
      if (isDeleted) {
        return res.ok();
      } else {
        return res.notFound();
      }
    } catch (e) {
      console.error(e);
      return res.serverError(e);
    }
  },
  /**
   * @swagger
   *
   * /getOne:
   *    summary: Get Note
   *    description: Get a new Note record with id
   *    externalDocs:
   *    responses:
   *      200:
   *        description: The requested resource
   *        content:
   *          application/json:
   *            schema:
   *              $ref: '#/components/schemas/note-unpopulated'
   */
  getOne: async (req, res) => {
    try {
      const note = await Note.findOne({
        id: req.params.id
      });
      if (note) {
        return res.json(note);
      } else {
        return res.notFound();
      }
    } catch (e) {
      console.error(e);
      return res.serverError(e);
    }
  }
};

