/**
 * @controller
 * UserController
 */

/**
 * @swagger
 *
 * /allActions:
 *  tags:
 *    - User
 *
 * components:
 *  securitySchemes:
 *    Connected:
 *      type: apiKey
 *      in: header
 *      name: Authorization
 *  schemas:
 *    user-without-password:
 *      properties:
 *        id:
 *          type: string
 *        login:
 *          type: string
 */

const {v4} = require('uuid');
module.exports = {
  /**
   * @swagger
   *
   * /auth:
   *  summary: Auth User
   *  description: Authenticate a User by login/password
   *  externalDocs:
   *  requestBody:
   *    required: true
   *    content:
   *      application/json:
   *        schema:
   *          properties:
   *            login:
   *              type: string
   *              required: true
   *            password:
   *              type: string
   *              format: password
   *              required: true
   *  responses:
   *    200:
   *      description: The requested resource
   *      content:
   *        application/json:
   *          schema:
   *            properties:
   *              token:
   *                type: string
   *              user:
   *                $ref: '#/components/schemas/user-without-password'
   */
  auth: async (req, res) => {
    try {
      if (req.body.login && req.body.password) {
        const connection = await sails.helpers.authByCredentials.with({
          login: req.body.login,
          password: req.body.password
        });
        if (connection) {
          return res.json(connection);
        } else {
          return res.notFound();
        }
      } else {
        return res.badRequest('Invalid fields');
      }
    } catch (e) {
      console.error(e);
      return res.serverError(e);
    }
  },
  /**
   * @swagger
   *
   * /create:
   *  summary: Create User
   *  description: Create a User with login/password
   *  externalDocs:
   *  requestBody:
   *    required: true
   *    content:
   *      application/json:
   *        schema:
   *          properties:
   *            login:
   *              type: string
   *              required: true
   *            password:
   *              type: string
   *              format: password
   *              required: true
   *  responses:
   *    200:
   *      description: The requested resource
   *      content:
   *        application/json:
   *          schema:
   *            $ref: '#/components/schemas/user-without-password'
   */
  create: async (req, res) => {
    try {
      if (req.body.login && req.body.password) {
        const user = await User.create({
          id: v4(),
          login: req.body.login,
          password: req.body.password
        }).fetch();
        delete user.password;
        return res.json(user);
      } else {
        return res.badRequest('Invalid fields');
      }
    } catch (e) {
      console.error(e);
      return res.serverError(e);
    }
  }
};

