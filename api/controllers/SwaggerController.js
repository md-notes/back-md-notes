/**
 * SwaggerController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const fs = require('fs');
const path = require('path');
module.exports = {
  json: async (req, res) => {
    try {
      console.log(path.resolve('swagger/swagger.json'));
      const swagger = fs.readFileSync(path.resolve('swagger/swagger.json'));
      return res.json(JSON.parse(swagger));
    } catch (e) {
      console.error(e);
      return res.serverError(e);
    }
  }
};

