/**
 * @controller
 * LeafController
 */

/**
 * @swagger
 *
 * /allActions:
 *  tags:
 *    - Leaf
 *  security:
 *    - Connected: []
 *
 * components:
 *  schemas:
 *    leaf-unpopulated:
 *      properties:
 *        id:
 *          type: string
 *        label:
 *          type: string
 *        parentLeaf:
 *          type: string
 *        user:
 *          type: string
 */

const {v4} = require('uuid');
module.exports = {
  /**
   * @swagger
   *
   * /create:
   *    summary: Create Leaf
   *    description: Create a new Leaf record linked to your account
   *    externalDocs:
   *    requestBody:
   *      content:
   *        application/json:
   *          schema:
   *            type: object
   *            properties:
   *              label:
   *                type: string
   *                required: true
   *              parentLeaf:
   *                type: string
   *    responses:
   *      200:
   *        description: The requested resource
   *        content:
   *          application/json:
   *            schema:
   *              $ref: '#/components/schemas/leaf-unpopulated'
   */
  create: async (req, res) => {
    try {
      if (req.body.label) {
        const leaf = await Leaf.create({
          id: v4(),
          label: req.body.label,
          parentLeaf: req.body.parentLeaf,
          user: req.connection.user.id
        }).fetch();
        if (leaf) {
          return res.json(leaf);
        } else {
          return res.notFound();
        }
      } else {
        return res.badRequest('Invalid fields');
      }
    } catch (e) {
      console.error(e);
      return res.serverError(e);
    }
  },
  /**
   * @swagger
   *
   * /update:
   *    summary: Update Leaf
   *    description: Update a new Leaf record linked to your account
   *    externalDocs:
   *    requestBody:
   *      content:
   *        application/json:
   *          schema:
   *            type: object
   *            properties:
   *              label:
   *                type: string
   *              parentLeaf:
   *                type: string
   *    responses:
   *      200:
   *        description: The requested resource
   *        content:
   *          application/json:
   *            schema:
   *              $ref: '#/components/schemas/leaf-unpopulated'
   */
  update: async (req, res) => {
    try {
      const leaf = await Leaf.updateOne({
        id: req.params.id
      }).set({
        label: req.body.label,
        parentLeaf: req.body.parentLeaf,
      });
      if (leaf) {
        return res.json(leaf);
      } else {
        return res.notFound();
      }
    } catch (e) {
      console.error(e);
      return res.serverError(e);
    }
  },
  /**
   * @swagger
   *
   * /delete:
   *    summary: Delete Leaf
   *    description: Delete a new Leaf record with id
   *    externalDocs:
   */
  delete: async (req, res) => {
    try {
      await Note.destroy({leaf: req.params.id});
      await Leaf.destroy({parentLeaf: req.params.id});
      const isDeleted = await Leaf.destroyOne({
        id: req.params.id
      });
      if (isDeleted) {
        return res.ok();
      } else {
        return res.badRequest();
      }
    } catch (e) {
      console.error(e);
      return res.serverError(e);
    }
  },
  /**
   * @swagger
   *
   * /getAllParents:
   *    summary: Get all parents Leaf
   *    description: Get parents Leaf (leaf with no parentLeaf) of your account
   *    externalDocs:
   *    responses:
   *      200:
   *        description: The requested resource
   *        content:
   *          application/json:
   *            schema:
   *              type: array
   *              items:
   *                $ref: '#/components/schemas/leaf-unpopulated'
   */
  getAllParents: async (req, res) => {
    try {
      const tree = await Leaf.find({
        user: req.connection.user.id,
        parentLeaf: null
      });
      if (tree) {
        return res.json(tree);
      } else {
        return res.notFound();
      }
    } catch (e) {
      console.error(e);
      return res.serverError(e);
    }
  },
  /**
   * @swagger
   *
   * /getOne:
   *    summary: Get Leaf
   *    description: Get Leaf with id
   *    externalDocs:
   *    responses:
   *      200:
   *        description: The requested resource
   *        content:
   *          application/json:
   *            schema:
   *              $ref: '#/components/schemas/leaf'
   */
  getOne: async (req, res) => {
    try {
      const leaf = await Leaf.findOne({id: req.params.id}).populate('notes').populate('children');
      if (leaf) {
        return res.json(leaf);
      } else {
        return res.notFound();
      }
    } catch (e) {
      console.error(e);
      return res.serverError(e);
    }
  }
};

